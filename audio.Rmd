---
title: "Audio"
author: "James Green"
date: "06/04/2021"
output: distill::distill_article
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Audio-tweaking in Shot-cut & OBS

Based on this video, I try and achieve the following
https://youtu.be/dQCB72S64L4

1. Equalization (High to about 6dB, Low to about 4 dB)
2. Amplify (Default values)
3. Compressor (-30dB, -50dB, 5:1, 0,10secs, 1.0secs)
4. Amplify (-3.00dB)